<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
    <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<!-- two line above depends on dependency javax servlet jtsl-->
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Hello world!!!</h1>
	<h2>${message}</h2>
	
	<%
		for(int i=0; i< 10 ;i++) {
			out.print(i);
		}
	%>
	
	<c:forEach var="j" begin="1" end="3">  
	   <p>Item <c:out value="${j}"/> </p>
	</c:forEach> 
</body>
</html>