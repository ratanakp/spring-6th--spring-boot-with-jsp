package com.example.demo;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@PropertySource("classpath:/i18n/message.properties")
public class MainController {
	
	@Value("${message1}")
	private String message = "Hi";
	
	@RequestMapping("index")
	public String index(Map<String, Object> model) {
		System.out.println(message);
		model.put("message", message);
		return "index";
	}

}
